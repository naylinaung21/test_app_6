class PagesController < ApplicationController
  def home
  end

  def about
  end

  def download_csv
    data = generate_csv_data

    respond_to do |format|
      format.csv do
        send_data data,
                  type: 'text/csv; charset=utf-8; header=present',
                  disposition: 'attachment; filename=example.csv'
      end
    end
  end

  private

  def generate_csv_data
    # Implement your logic to generate CSV data here
    CSV.generate { |csv| csv << ['Header 1', 'Header 2'] }
  end
  
end
