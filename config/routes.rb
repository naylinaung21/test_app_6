Rails.application.routes.draw do
  # get 'pages/home'
  get 'about', to: "pages#about"
  get 'download_csv', to: "pages#download_csv"
  resources :articles, only: [ :index, :show, :new, :create ]
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "pages#home"
end
